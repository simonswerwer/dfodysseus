# DFOdysseus
###A 3D visualizer focusing on recreating the landscapes created in Dwarf Fortress. For exploration purposes.

DFOdysseus uses the Unreal Engine to parse DF map exports. At the moment I am only including the source. It is usable, but only if you have Unreal Engine 4.12 installed. 

![LandscapeSample.png](https://bitbucket.org/repo/qBAaLA/images/4126211378-LandscapeSample.png)



####Features:
* Up to 100kmx100km map exploration.
* See dwarven fortresses, human towns and big tre...er "forest retreats".
* Grassland, forest, generic dirt and ocean biomes.
* One dwarf character.
* A "camp fire" (logs and a fire particle effect).
* Rudimentary procedural forests, rivers, seas and mountain ranges.
* Day-night cycle.
* DFArgos (a C# utility used for parsing map files. Not included yet, but source will be available soon).
* I've included the original world map exports (DFExports/SourceFiles/) and the processed images (DFExports/).

####Current Bugs:
Unfortunately, I don't currently have the time (nor, to be honest, the patience) to figure out how to overcome the following bugs (due to UE4 weirdness, lack of documentation and my own ignorance).

* Grass floats in the air in some places.
* Grass has empty patterned square patches.
* Black spots on the ground where settlements are.
* Some broken/ugly imposters.
* Dwarf model clips atrociously and is creepy, I'm no 3d artist lol.
* Town mesh needs a texture.
* Smaller lakes don't appear.
* Probably not very fast on low end pc's at the moment, but you could try reducing overall quality settings.
* Weird blueprint runtime error on exit.
* Sometimes river texture shifts on start (e.g. rivers over mountain ranges).

####To-do(?):
* A rideable pony
* Swimming
* Better meshes
* Optimizations galore
* Collisions with objects
* Sound effects
* Survival mechanics
* More biomes
* More structures
* NPC's (hahaha)
* Caves (might be possible, seriously)
* Directory cleanup (infuriating to do with UE4)

####Notes:
* This repository is very close to hitting the 2GB cap, so sorry about that. Perhaps someone can suggest some files/folders for the .gitignore file? I tried git LFS on Github and almost died, so no Github.
* All assets are from the UE4 Starter Kit and Open World demo, except for the dwarf and structures.
* Tinkering welcome, otherwise instructions on how to use it are on the way.


###The MIT License (MIT)

Copyright (c) 2016 Simon Swerwer

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.