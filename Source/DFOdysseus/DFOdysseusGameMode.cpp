// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "DFOdysseus.h"
#include "DFOdysseusGameMode.h"
#include "DFOdysseusCharacter.h"

ADFOdysseusGameMode::ADFOdysseusGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Character/Dwarf/ss_ArmoredDwarf_Character"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
