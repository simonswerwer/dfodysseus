// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "DFOdysseusGameMode.generated.h"

UCLASS(minimalapi)
class ADFOdysseusGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ADFOdysseusGameMode();
};



